# Subclaro

A not so imaginative name for a useful admin theme. This is the spiritual
successor of [Subseven](https://www.drupal.org/project/subseven).

## BACKGROUND
This theme was spawned from the changes that were always applied to new Drupal
projects in our team and were not specific to those projects.

Subclaro doesn't use the latest/fanciest toolkit around for CSS, it is just plain
old CSS with easy to write and read declarations that will make your daily
Drupal administration life easier.

## IMPORTANT
- Subclaro displays the views preview area on the side on big screens.
  This will be removed from the theme once/if this gets added into core.
  More info: https://www.drupal.org/project/drupal/issues/3185775

# CREDITS
Initial development: Bill Seremetis (bserem)
Sponsoring: annertech.com
